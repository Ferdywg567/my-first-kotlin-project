package com.example.kotlinapp

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class SlidePagerAdapter(
    FragmentManager: FragmentManager
) : FragmentStatePagerAdapter(FragmentManager) {

    override fun getItem(position: Int): Fragment {
        return if(position == 0) Slide1Fragment()
        else if(position == 1) Slide2Fragment()
        else Slide3Fragment()

    }

    override fun getCount(): Int {
        return 3
    }

}